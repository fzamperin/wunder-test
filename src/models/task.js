export default (sequelize, DataTypes) => {
  let Task = sequelize.define('Task', {
    initialvalue: {
      allowNull: false,
      type: DataTypes.STRING
    },
    result: {
      allowNull: true,
      type: DataTypes.STRING
    },
    iscompleted: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    },
    rounds: {
      allowNull: true,
      type: DataTypes.INTEGER
    }
  },
    {
      freezeTableName: true
    }
  );

  Task.associate = (models) => {
    Task.Batch = Task.belongsTo(models.Batch);
  };

  return Task;
};