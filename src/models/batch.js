export default (sequelize, DataTypes) => {
  let Batch = sequelize.define('Batch', {
    iscompleted: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false
    }
  },
    {
      freezeTableName: true
    }
  );

  Batch.associate = (models) => {
    Batch.Tasks = Batch.hasMany(models.Task, {
      as: 'Tasks',
      foreignKey: {
        allowNull: false
      }
    });
  };

  return Batch;
};
