import gulp from 'gulp';
import db from './models';

gulp.task('sequelize:drop', () => {
  db.sequelize.sync({ force: true }).then(() => process.exit(0));
});

gulp.task('sequelize', () => {
  db.sequelize.sync().then(() => process.exit(0));
});
