import websocket from 'ws';
import uuid from 'uuid/v4';
import socketHandler from './helpers/socket-handler';

const wss = new websocket.Server({ port: 3000 });

wss.on('connection', (ws) => {
  console.log('ENV', process.env);
  ws.id = uuid();
  ws.on('message', async (payload) => {
    try {
      const payloadObject = JSON.parse(payload);
      await socketHandler.handleMessage(payloadObject, ws);
    }
    catch (err) {
      console.log(err);
      ws.send(err.message);
    }
  });
  ws.send('Successfully connected please send the phrases you want to crypt, each phrase separated by a newline');
});
