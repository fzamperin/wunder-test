/** This Class represents the actions that the socket would call (Singleton) */
class Actions {
  CREATE_BATCH() { return 'create:batch' };
  FIND_ALL_BATCHES() { return 'find:batches' };
  SUBSCRIBE_TO_BATCH() { return 'subscribe:batch' };
  GET_BATCH_BY_ID() { return 'find:batch' }
}

let instance = new Actions();

export default instance;