import Actions from './actions';
import Queue from './queue';
import db from '../models';

/** This Class is the socker handler for the connection (Singleton) */
class SocketHandler {

  constructor() {
    this.queues = [];
    process.on('uncaughtException', () => {
      let array = [];
      for(let queue of this.queues) {
        array.push(queue.close(30 * 1000));
      }
      Promise.all(array).then(() => {
        process.exit(1);
      });
    });
  }

  async handleMessage(payload, ws) {
    try {
      if(payload.action === Actions.CREATE_BATCH()) {
        await this.createBatch(payload, ws);
      } else if(payload.action === Actions.FIND_ALL_BATCHES()) {
        await this.findAllBatches(ws);
      } else if(payload.action === Actions.SUBSCRIBE_TO_BATCH()) {
        await this.subscribeToBatchById(payload, ws);
      } else if(payload.action === Actions.GET_BATCH_BY_ID()) {
        await this.getBatchById(payload.id);
      }
    }
    catch (err) {
      throw err;
    }
  }

  async createBatch(payload, ws) {
    try {
      let batch = await db.Batch.create(payload.batch, { include: [ db.Batch.Tasks ] });
      let queue = new Queue(batch);
      await queue.initializeJobs();
      this.queues.push(queue);
      this.addHook(batch, ws);
      this.onClose(ws);
      await queue.startProcessing();
      this.sendToSocket({ message: `Batch: ${batch.id}, has started processing.` }, ws);
    }
    catch (err) {
      throw err;
    }
  }

  async findAllBatches(ws) {
    let batches = await db.Batch.findAll({
      include: [
        {
          model: db.Task,
          as: 'Tasks',
          required: false
        }
      ]
    });
    this.sendToSocket(batches, ws);
  }

  async getBatchById(id, ws) {
    let batch = await db.Batch.findByPk(id, {
      include: [
        {
          model: db.Taks,
          required: false
        }
      ]
    });
    this.sendToSocket(batch, ws);
  }

  async subscribeToBatchById(payload, ws) {
    try {
      let batch = await db.Batch.findByPk(payload.batchid, {
        include: [
          {
            model: db.Task,
            as: 'Tasks',
            required: true,
            where: {
              iscompleted: true
            }
          }
        ]
      })
      if(batch) {
        this.addHook(batch, ws);
        const body = {
          message: 'Batch subscribed with success',
          batch: batch
        }
        this.sendToSocket(body, ws);
        this.onClose(ws);
      }
      else {
        ws.send(`Batch id ${payload.batchid} not found or has completed processing`);
      }
    }
    catch (err) {
      throw err;
    }
  }

  sendToSocket(body, ws) {
    ws.send(JSON.stringify(body));
  }

  onClose(ws) {
    ws.on('close', () => {
      this.removeHook(batch, ws);
    });
  }

  addHook(batch, ws) {
    db.Task.addHook('afterUpdate', `${batch.id}-${ws.id}`, async (task) => {
      const body = {
        message: 'Finished processing task',
        task: task.get()
      }
      this.sendToSocket(body, ws);
      let countRemainingTasks = await db.Task.count({
        where: {
          BatchId: batch.id,
          iscompleted: false
        }
      })
      if(countRemainingTasks === 0) {
        db.Task.removeHook('afterUpdate', `${batch.id}-${ws.id}`);
        let index = this.queues.findIndex(queue => queue.batch.id === batch.id);
        let queue = this.queues[index];
        queue.queue.destroy();
        batch.update({ iscompleted: true });
        this.queues.splice(index, 1);
      }
    });
  }

  removeHook(batch, ws) {
    db.Task.removeHook('afterUpdate', `${batch.id}-${ws.id}`);
  }

  async onInit() {
    try {
      let batches = await db.Batch.findAll({
        where: {
          iscompleted: false
        }
      })
      for(let batch of batches) {
        await this.resumeBatch(batch);
      }
    }
    catch (err) {
      throw err;
    }
  }

  async resumeBatch(batch) {
    try {
      let queue = new Queue(batch);
      this.queues.push(queue);
      await queue.startProcessing();
    }
    catch (err) {
      throw err;
    }
  }

}

let instance = new SocketHandler();

// Search for jobs that hasn't finished
instance.onInit();

export default instance;