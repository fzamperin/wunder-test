import Bee from 'bee-queue';
import bcrypt from 'bcrypt';
import db from '../models';

/** This Class represents a Queue */
export default class Queue {


  /**
     * Create a queue.
     * @param {Object} Batch - The batch inserted by the user.
  */
  constructor(batch) {
    this.batch = batch;
    this.queue = new Bee(batch.id, {
      prefix: 'crypto',
      stallInterval: 5000,
      nearTermWindow: 1200000,
      delayedDebounce: 1000,
      redis: {
        host: 'redis',
        port: process.env.REDIS_PORT,
        db: 0,
        options: {}
      },
      isWorker: true,
      getEvents: true,
      sendEvents: true,
      storeJobs: true,
      ensureScripts: true,
      activateDelayedJobs: false,
      removeOnSuccess: false,
      removeOnFailure: false,
      redisScanCount: 100
    });
  }
  /**
    * Initialize the jobs based on a list of tasks in side the batch object
  */
  async initializeJobs() {
    try {
      for (let task of this.batch.Tasks) {
        const job = await this.queue.createJob({
          id: task.id
        });
        await job.timeout(15000).retries(3).save();
      }
      await this.queue.ready();
    }
    catch (err) {
      throw err;
    }
  }

  /**
     * Start processing the list after the jobs were inserted
     * @return {Promise<any>} The promise from the process function
  */
  async startProcessing() {
    return this.queue.process(process.env.CONCURRENT_JOBS, async (job) => {
      const task = await db.Task.findByPk(job.data.id);
      const crypto = await bcrypt.hash(task.initialvalue, 16);
      task.result = crypto;
      task.rounds = 16;
      task.iscompleted = true;
      await task.save();
      return task.get({ plain: true });
    })
  }

  /**
     * Destroy the queue and eliminates it from the redis db
     * @return {Promise<any>} The promise resultant from the destroy function
  */
  destroy() {
    return this.queue.destroy();
  }

}
