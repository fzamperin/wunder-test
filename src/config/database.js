const config = {
    username: process.env['DATABASE_USERNAME'] || "root",
    password: process.env['DATABASE_PASSWORD'] || "123456",
    database: process.env['DATABASE'] || "wunder",
    host: process.env['DATABASE_HOST'] || "127.0.0.1",
    port: process.env['DATABASE_PORT'] || 3306,
    dialect: "mysql",
    define: {
      charset: "utf8mb4",
      dialectOptions: {
        collate: "utf8mb4_general_ci"
      }
    }
  };
  
  export default config;
  