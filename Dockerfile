FROM node:carbon

WORKDIR /usr/app

COPY . .
RUN npm install --quiet

## Add the wait script to the image
ADD https://github.com/ufoscout/docker-compose-wait/releases/download/2.4.0/wait /wait
RUN chmod +x /wait

## Launch the wait tool and then your application
CMD /wait && npm run create:database && npm run prod