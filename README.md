# Wunder-Socket-Test

A simple application that you can send tasks to crypto your phrases, you open a connection with the server, send you phrases based on a Batch (Batch of tasks), and then the server responds with the progress, if you uploaded a Batch and your connection go down, don't be afraid you can continue from where you were and get the events from the result.

## Developed using:
- Developed using ws library (Web-sockets);
- Uses Sequelize and MariaDB for database;
- Redis for Queue database;
- It counts with Babel and es2015 presets for es6 syntax;
- Eslint based on Airbnb;

## Features
- List all the Batches available in the database;
- Subscribe to a batch that is processing to receive the events;
- When the server restarts for any reason, it restore the jobs that were running;
- And get a Batch by Id;

## Instalation
First you need to install nodejs 8+, to run the project execute the following commands:

```sh
$ npm i
```
Creates the database (Mysql or MariaDB), the executes the command inside the gulpfile to create database tables

```sh
$ npm run create:database
```

And then, to run in development mode:

```sh
$ npm run dev
```

### Extras
There's a docker-compose file if you would like to start the environment in a easy way, docker needs to be installed.
Executes the command:

```sh
$ docker-compose up -d
```
